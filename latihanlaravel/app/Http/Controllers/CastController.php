<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create() {
        return view("cast.create");
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'cast_name' => 'required',
            'cast_age' => 'required',
            'cast_bio' => 'required',
        ],
        [
            'cast_name.required' => 'Nama wajib Diisi',
            'cast_age.required'  => 'Umur wajib Diisi',
            'cast_bio.required' => 'Bio wajib Diisi',

        ]

        );

        DB::table('cast')->insert(
        [
            'cast_name' => $request['cast_name'], 
            'cast_age' => $request['cast_age'],
            'cast_bio' => $request['cast_bio']
        ]
        );

        return redirect('/cast');

    }

    public function tampil() {
        $cast = DB::table('cast')->get();
 
        return view('cast.tampil', compact('cast'));
    }

    public function show($cast_id) {
        $cast = DB::table('cast')->where('cast_id', $cast_id)->first();

        return view('cast.show',compact('cast'));

    }

    public function edit($cast_id) {
        $cast = DB::table('cast')->where('cast_id', $cast_id)->first();

        return view('cast.edit',compact('cast'));
    }

    public function update($cast_id, Request $request) {
        $validatedData = $request->validate([
            'cast_name' => 'required',
            'cast_age' => 'required',
            'cast_bio' => 'required',
        ],
        [
            'cast_name.required' => 'Nama wajib Diisi',
            'cast_age.required'  => 'Umur wajib Diisi',
            'cast_bio.required' => 'Bio wajib Diisi',

        ]

        );

        DB::table('cast')
              ->where('cast_id', $cast_id)
              ->update([
                  'cast_name'=>$request['cast_name'],
                  'cast_age'=>$request['cast_age'],
                  'cast_bio'=>$request['cast_bio']
              ]);
        return redirect('/cast');
    }

    public function destroy($cast_id) {
        DB::table('cast')->where('cast_id','=',$cast_id)->delete();

        return redirect('/cast');

    }
}
