<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/data-table',function() {
    return view('halaman.data-table');
} );

Route::get('/table',function() {
    return view('halaman.table');
} );

Route::get('/home',function() {
    return view('home');
} );

Route::get('/','HomeController@dashboard');
Route::get('/register','AuthController@daftar');

Route::post('/kirim','AuthController@welcome');

//CRUD Cast Table Perfilman

//input data baru
Route::get("/cast/create","CastController@create");
//meyimpan data ke database
Route::post("/cast","CastController@store");
//menampilkan data yang ada
Route::get('/cast','CastController@tampil');
//melihat detail data
Route::get('/cast/{cast_id}','CastController@show');
//edit data
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
//delete data
Route::delete('/cast/{cast_id}','CastController@destroy');
