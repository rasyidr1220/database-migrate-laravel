@extends('layout.master')

@section('judul')
    <h1>Halaman Casting Film</h1>
@endsection

@section('content')
<h2>Halaman untuk mengedit casting film</h2>
<form action="/cast/{{$cast->cast_id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama Casting</label>
      <input type="text" value="{{$cast->cast_name}}" class="form-control" name="cast_name">
    </div>
    @error('cast_name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" value="{{$cast->cast_age}}" class="form-control" name="cast_age">
    </div>
    @error('cast_age')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="cast_bio" class="form-control" id="" cols="30" rows="10" value="{{$cast->cast_bio}}"></textarea>
    </div>
    @error('cast_bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection