@extends('layout.master')

@section('judul')
    <h1>Halaman Casting Film</h1>
@endsection

@section('content')

<h2>Casting Film yang telah diambil</h2>

<a href="/cast/create" class="btn btn-primary my-3">Tambah Casting Baru</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Casting</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->cast_name}}</td>
            <td>{{$item->cast_age}}</td>
            <td>{{$item->cast_bio}}</td>
            <td>
                <form action="/cast/{{$item->cast_id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->cast_id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->cast_id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                </form>
            </td>
        </tr>
         
     @empty
         <h2>Data Kosong</h2>
     @endforelse
    </tbody>
  </table>
    
@endsection