@extends('layout.master')

@section('judul')
    <h1>Halaman Casting Film</h1>
@endsection

@section('content')
<h2>Halaman untuk menambah casting film</h2>
<form action="/cast" method="POST">
  @csrf
    <div class="form-group">
      <label>Nama Casting</label>
      <input type="text" class="form-control" name="cast_name">
    </div>
    @error('cast_name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control" name="cast_age">
    </div>
    @error('cast_age')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="cast_bio" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('cast_bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection