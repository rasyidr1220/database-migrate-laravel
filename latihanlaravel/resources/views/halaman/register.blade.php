@extends('layout.master')

@section('judul')

<h1>Buat Akun Baru</h1>
    
@endsection

@section('content')

<h2>Sign Up Form</h2>
<form action="/kirim" method="post">
    @csrf
    <label>First Name</label> <br>
    <input type="text" name = "fnama"> <br>
    <label>Last Name</label> <br>
    <input type="text" name = "lnama"> <br> <br>
    <label>Gender</label> <br>
    <input type="radio" value="1" name="jk"> Male <br>
    <input type="radio" value="2" name="jk"> Female <br><br>
    <label>Nationality</label> <br>
    <select name="nation" id="">
        <option value="">Indonesia</option>
        <option value="">United States</option>
        <option value="">Japanese</option>
        <option value="">United Kingdom</option>
    </select> <br><br>
    <label>Language Speak</label> <br>
    <input type="checkbox"> Indonesia <br>
    <input type="checkbox"> Japanese <br>
    <input type="checkbox"> English <br> <br>
    <label>Bio</label> <br>
    <textarea name="" id="" cols="30" rows="10"></textarea> <br> <br>
    <input type="submit" value="Sign Up">
</form>

    
@endsection

   